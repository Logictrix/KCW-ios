//
//  ViewCalendarCell.swift
//  KCW
//
//  Created by Ankur sharma on 26/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class ViewCalendarCell: UITableViewCell
{
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblClientName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewCell: UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
