//
//  ViewTimeTableViewCell.swift
//  KCW
//
//  Created by Ankur sharma on 27/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class ViewTimeTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblCase: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
