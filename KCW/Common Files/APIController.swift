//
//  APIController.swift
//  BrainCal
//
//  Created by Logictrix iOS3 on 24/07/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class APIController: NSObject {
    
    static let appdel  = UIApplication.shared.delegate as! AppDelegate

    static func authenticate(staffno:String,password:String, SuccessHandler: @escaping (_ responce:JSON) -> Void)
    {
        var params = [String:Any]()
        params["staffno"] = staffno
        params["password"] = password
        print(params)
        ServiceManagerClass.requestWithPost(methodName: "/PrimeAsmx.asmx/Authenticate", parameter: params, Auth_header: [:]) { (jsonResponse) in
            print(jsonResponse)
            if(jsonResponse["status"].string == "1")
            {
                SuccessHandler(jsonResponse)
            }
            else
            {
                ServiceManagerClass.alert(message: jsonResponse["message"].string!)
            }
        }
    }
    
    static func getCase(staffno:String, SuccessHandler: @escaping (_ responce:JSON) -> Void)
    {
        var params = [String:Any]()
        params["staffno"] = staffno
        print(params)
        ServiceManagerClass.requestWithPost(methodName: "/PrimeAsmx.asmx/GetCase", parameter: params, Auth_header: [:]) { (jsonResponse) in
            print(jsonResponse)
            if(jsonResponse["status"].string == "1")
            {
                SuccessHandler(jsonResponse)
            }
            else
            {
                ServiceManagerClass.alert(message: jsonResponse["message"].string!)
            }
        }
    }
    
    static func urlauthentication(pass:String, SuccessHandler: @escaping (_ responce:JSON) -> Void)
    {
        var params = [String:Any]()
        params["pass"] = pass
        print(params)
        ServiceManagerClass.requestWithPost(methodName: "/PrimeAsmx.asmx/UrlAuthentication", parameter: params, Auth_header: [:]) { (jsonResponse) in
            print(jsonResponse)
            if(jsonResponse["status"].string == "1")
            {
                SuccessHandler(jsonResponse)
            }
            else
            {
                ServiceManagerClass.alert(message: jsonResponse["message"].string!)
            }
        }
    }
    
    static func saveCalendar(startdate:String, starttime:String, enddatetime:String, subject:String, casenum:String, userid:String, SuccessHandler: @escaping (_ responce:JSON) -> Void)
    {
        var params = [String:Any]()
        params["startdate"] = startdate
        params["starttime"] = starttime
        params["enddatetime"] = enddatetime
        params["subject"] = subject
        params["casenum"] = casenum
        params["userid"] = userid
        print(params)
        ServiceManagerClass.requestWithPost(methodName: "/PrimeAsmx.asmx/SaveCalender", parameter: params, Auth_header: [:]) { (jsonResponse) in
            print(jsonResponse)
            if(jsonResponse["status"].string == "1")
            {
                SuccessHandler(jsonResponse)
            }
            else
            {
                ServiceManagerClass.alert(message: jsonResponse["message"].string!)
            }
        }
    }

    func imageUplode( image:UIImage, successHandler: @escaping (_ responce:JSON) -> Void)
    {
        let parameters: [String:Any] =
            [:]
        print(parameters)
        ServiceManagerClass.requestWithPostMultipart(methodName: "UploadImage.aspx", image: image, parameter: parameters) { (jsonResponse) in
            print(jsonResponse)
            if(jsonResponse["status"].string == "1")
            {
                successHandler(jsonResponse)
            }
            else
            {
                successHandler(jsonResponse)
                // ServiceManager.alert(message: jsonResponse["message"].stringValue)
            }
        }
    }
    
    static func GetArticle(SuccessHandler: @escaping (_ responce:JSON) -> Void)
    {
        let params = [String:Any]()
        ServiceManagerClass.requestWithPost(methodName: "GetArticle", parameter: params, Auth_header: [:]) { (jsonResponse) in
            print(jsonResponse)
            if(jsonResponse["status"].string == "1")
            {
                SuccessHandler(jsonResponse )
            }
            else
            {
                ServiceManagerClass.alert(message: jsonResponse["message"].string!)
            }
        }
    }
    
}
