//
//  ServiceManagerClass.swift
//  BrainCal
//
//  Created by Logictrix iOS3 on 24/07/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD
import MBProgressHUD

class ServiceManagerClass: NSObject {

  static  let baseUrl =  "http://50.62.35.14:8181/PrimeAsmx.asmx?"

    static  let ImageUrl =  "http://50.62.35.14:8090/UploadImage.aspx/"
    
    class func requestWithPost(methodName:String, parameter:[String:Any]?,Auth_header:[String:String], successHandler: @escaping(_ success:JSON) -> Void)
    {
       // CommonVC.showHUD()
        let parameters: Parameters = parameter!
        var jsonResponse:JSON!
        var errorF:NSError!
        print(errorF)
        let urlString = baseUrl.appending("\(methodName)")
        print("parameters",parameters)
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        //"text/plain; charset=utf-8"
     //   URLRequest.setValue("application/json",
               //             forHTTPHeaderField: "Content-Type")
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        
        print("urlString",urlString)
        sessionManager.request(urlString, method: .post, parameters: parameters, encoding:URLEncoding.httpBody , headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch response.result{
            case .failure(let error):
                print(error.localizedDescription)
                errorF = error as NSError
                //self.hideLoadingHUD()
                ServiceManagerClass.alert(message: error.localizedDescription)
                break
            case .success(let value):
                print("value",value)
//                print("response.request",response.request!)  // original URL request
//                print("response",response.response!) // HTTP URL response
             //   print("ggg",response.data!)     // server data
              //  print("result",response.result)   // result of response serialization
                
                do{
                    let json = try JSON(data: response.data!)
                   print("json",json)
                    let jsonType = [json] as NSArray
                    print("jsonType",jsonType)
                    jsonResponse = json
                    break
                }
                catch{
                    print("error",error.localizedDescription)
                }
               
            }
            if jsonResponse !=  nil{
                    successHandler(jsonResponse)
            }
                
                
            else{
           //     ServiceManagerClass.alert(message: errorF.localizedDescription)
            }
           // KRProgressHUD.dismiss()
            SVProgressHUD.dismiss()
            sessionManager.session.invalidateAndCancel()
        }
    }
    
    
    
    class func requestWithPostMultipart(methodName:String , image:UIImage, parameter:[String:Any]?, successHandler: @escaping (_ success:JSON) -> Void)
    {
            let parameters: Parameters = parameter!
            var jsonResponse:JSON!
            let urlString = ImageUrl.appending("\(methodName)")
            
            // let image = UIImage(named: "image.png")
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(UIImageJPEGRepresentation(image, 0.5)!, withName: "photo_path", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to: urlString, method: .post , headers:nil, encodingCompletion: { (result) in
                switch result {
                case .success(let upload,_,_):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print(progress.fractionCompleted * 100)
                    })
                    
                    upload.responseJSON(completionHandler: { (response) in
                        let json = JSON(data: response.data!)
                        print("\(json)")
                        jsonResponse = json
                        //   indicator.stopAnimating()
                        successHandler(jsonResponse)
                    })
                case .failure(let error):
                    print(error)
 
                }
            })
//        }
//        else
//        {
//            ServiceManager.alert(message: "Network is Unreachable")
//        }
    }
    
    
    
    
    
//    class func requestWithGet(methodName:String , parameter:[String:Any]?, successHandler: @escaping (_ success:JSON) -> Void) {
//        let errorDict:[String:Any] = [:]
//       // var errorJson:JSON = JSON(errorDict)
//        if !(methodName == "api/core/get_nonce/?controller=user&method=register"){
//              CommonVC.showHUD()
//        }
//
//            var jsonResponse:JSON!
//            let urlString = baseUrl.appending("\(methodName)")
//            print(urlString)
//
//            Alamofire.request(urlString, method: .get, parameters:[:], encoding: URLEncoding.default).responseJSON { (response:DataResponse<Any>) in
//                switch response.result{
//                case .failure(let error):
//                    print(error)
//                    //  errorJson = ["status":"Failed","message":error.localizedDescription]
//                    // SVProgressHUD.dismiss()
//                    // successHandler(errorJson)
//                    ServiceManagerClass.alert(message: error.localizedDescription)
//                    break
//                case .success(let value):
//                    print(value)
//                    print(response.request!)  // original URL request
//                    print(response.response!) // HTTP URL response
//                    print(response.data!)     // server data
//                    print(response.result)   // result of response serialization
//
//                    let json = JSON(data: response.data!)
//                    print("\(json)")
//                    jsonResponse = json
//                    successHandler(jsonResponse)
//                    break
//                }
//                SVProgressHUD.dismiss()
//        }
////        else
////        {
////            errorJson = ["status":0,"message":"Network is Unreachable"]
////            successHandler(errorJson)
////            ServiceManagerClass.alert(message: "Network is Unreachable")
////        }
//    }
    
    
    
    
    class func topMostController() -> UIViewController {
        var topController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController

        while ((topController?.presentedViewController) != nil) {
            topController = topController?.presentedViewController
        }
        return topController!
    }

    
    
    
    class func alert(message:String){
        let alert=UIAlertController(title: "MamasApp", message: message, preferredStyle: .alert);
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in

        }
        alert.addAction(cancelAction)

        print(ServiceManagerClass.topMostController())
        ServiceManagerClass.topMostController().present(alert, animated: true, completion: nil);

    }
//    class func alert(message:String){
//        let alert=UIAlertController(title: "Alert", message: message, preferredStyle: .alert);
//        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
//            
//        }
//        alert.addAction(cancelAction)
//        ServiceManager.topMostController().present(alert, animated: true, completion: nil);
//    }
    
//    fileprivate func showLoadingHUD() {
//        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
//        hud?.labelText = "vent venligst"
//    }
//    fileprivate func hideLoadingHUD() {
//        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
//    }
    class func UserDetials() -> NSDictionary
    {
        let data = UserDefaults.standard.value(forKey: "LoginUserData")
        let dict:NSDictionary = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as! NSDictionary
        print(dict as Any)

        return dict
    }

}
