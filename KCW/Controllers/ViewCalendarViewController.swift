//
//  ViewCalendarViewController.swift
//  KCW
//
//  Created by Ankur sharma on 26/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class ViewCalendarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var txtChooseDate: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewDate: UIView!
    var arrTime = ["10:00 a.m. to 11:00 a.m", "11:00 a.m. to 12:00 p.m", "12:00 p.m. to 01:00 p.m", "01:00 p.m. to 02:00 p.m", "02:00 p.m. to 03:00 p.m"]
    var arrClientName = ["Mark Michael", "Daniel Parera", "John Desuza", "Tony Jackson", "Tim Carry"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewDate.layer.borderWidth = 1
        viewDate.layer.borderColor = UIColor.black.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = false
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ViewCalendarCell") as! ViewCalendarCell
        cell.lblTime.text = arrTime[indexPath.row]
        cell.lblClientName.text = arrClientName[indexPath.row]
        cell.lblDescription.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        cell.imgProfile.layer.cornerRadius = 20
        cell.viewCell.layer.borderColor = UIColor.black.cgColor
        cell.viewCell.layer.borderWidth = 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CaseDetailsViewController") as! CaseDetailsViewController
        navigationController?.pushViewController(obj, animated: true)
    }
}
