//
//  CategoryViewController.swift
//  KCW
//
//  Created by Ankur sharma on 25/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func btnAddCalendar(_ sender: Any)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddCalendarViewController") as! AddCalendarViewController
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnViewCalendar(_ sender: Any)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewCalendarViewController") as! ViewCalendarViewController
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnAddTime(_ sender: Any)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddTimeViewController") as! AddTimeViewController
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnViewTime(_ sender: Any)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewTimeViewController") as! ViewTimeViewController
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnLogout(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
}
