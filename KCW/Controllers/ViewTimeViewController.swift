//
//  ViewTimeViewController.swift
//  KCW
//
//  Created by Ankur sharma on 27/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class ViewTimeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tblView: UITableView!
    var arrHours = ["Hours: 0.50", "Hours: 0.75", "Hours: 0.50", "Hours: 1.00", "Hours: 1.25", "Hours: 1.50", "Hours: 1.75", "Hours: 2.25", "Hours: 2.50"]
    var arrCase = ["Case #: 123", "Case #: 124", "Case #: 125", "Case #: 126", "Case #: 127", "Case #: 128", "Case #: 129", "Case #: 130"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblView.layer.borderWidth = 1
        tblView.layer.borderColor = UIColor.lightGray.cgColor
        tblView.delegate = self
        tblView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ViewTimeTableViewCell") as! ViewTimeTableViewCell
        cell.lblHours.text = arrHours[indexPath.row]
        cell.lblCase.text = arrCase[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj = storyboard?.instantiateViewController(withIdentifier: "TimeDetailsViewController") as! TimeDetailsViewController
        navigationController?.pushViewController(obj, animated: true)
    }
    
}
