//
//  LoginViewController.swift
//  KCW
//
//  Created by Ankur sharma on 25/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController
{
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRemember: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    var staff_num = ""
    var password = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtUsername.leftViewMode = UITextFieldViewMode.always
        txtPassword.leftViewMode = UITextFieldViewMode.always
        let bottomLineUrl = CALayer()
        bottomLineUrl.frame = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
        bottomLineUrl.backgroundColor = UIColor.black.cgColor
        txtUsername.borderStyle = UITextBorderStyle.none
        txtUsername.layer.addSublayer(bottomLineUrl)
        let bottomLinePass = CALayer()
        bottomLinePass.frame = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
        bottomLinePass.backgroundColor = UIColor.black.cgColor
        txtPassword.borderStyle = UITextBorderStyle.none
        txtPassword.layer.addSublayer(bottomLinePass)
        btnLogin.layer.cornerRadius = 24
        print(ServiceManagerClass.baseUrl)
        if UserDefaults.standard.value(forKey: "baseUrl") != nil
        {
            ServiceManagerClass.baseUrl = UserDefaults.standard.value(forKey: "baseUrl") as! String
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = true
        print(ServiceManagerClass.baseUrl)
        if ServiceManagerClass.baseUrl.isEmpty == true || ServiceManagerClass.baseUrl == ""
        {
            let alert=UIAlertController(title: "KCW", message: "Set the Proper URL to use the Services", preferredStyle: .alert);
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "EnterURLviewController") as! EnterURLviewController
                self.navigationController?.pushViewController(obj, animated: true)
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil);

        }
    }
    
    func callAPI()
    {
        if !validate()
        {
            return
        }
        self.staff_num = txtUsername.text!
        self.password = txtPassword.text!
        UserDefaults.standard.set(self.staff_num, forKey: "staffNum")
        UserDefaults.standard.set(self.password, forKey: "password")
        UserDefaults.standard.synchronize()
        APIController.authenticate(staffno: staff_num, password: password)
        {
            (response) in
            print(response)
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func validate() -> Bool
    {
        if txtUsername.text == ""
        {
            alert(msg: "Please enter username")
            return false
        }
        if txtPassword.text == ""
        {
            alert(msg: "Please enter password")
            return false
        }
        return true
    }
    
    func alert(msg:String)
    {
        let alt = UIAlertController(title: "Rentopolous", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { alert in
            print("OK Logic Here")
            self.dismiss(animated: true, completion: nil)
        })
        
        let CancelAction = UIAlertAction(title: "CANCEL", style: .default, handler: { alert in
            print("CANCEL Logic Here")
            self.dismiss(animated: true, completion: nil)
        })
        //        alt.addAction(CancelAction)
        alt.addAction(okAction)
        present(alt, animated: true, completion: nil)
    }

    @IBAction func btnLoginAction(_ sender: Any)
    {
        callAPI()
        
    }
    
    @IBAction func btnSettings(_ sender: Any)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "EnterURLviewController") as! EnterURLviewController
        navigationController?.pushViewController(obj, animated: true)
    }
    
}
