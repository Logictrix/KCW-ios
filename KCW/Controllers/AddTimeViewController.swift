//
//  AddTimeViewController.swift
//  KCW
//
//  Created by Ankur sharma on 26/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class AddTimeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate
{
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var txtCaseNumber: UITextField!
    var pickerData = ["C123: Mark Michael (Code: SAL123)" , "C456: John Cina (Code: SAL456)" , "C123: Mark Maurya (Code: SAL123)" , "C456: John Cina (Code: SAL456)", "C123: Mark Maurya (Code: SAL123)" , "C456: John Cina (Code: SAL456)"]
    var myPickerView : UIPickerView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtCaseNumber.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = false
    }

    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtCaseNumber.inputAccessoryView = toolBar
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.txtCaseNumber.text = pickerData[row]
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.pickUp(txtCaseNumber)
    }
    
    @objc func doneClick()
    {
        txtCaseNumber.resignFirstResponder()
    }
    
    @objc func cancelClick()
    {
        txtCaseNumber.resignFirstResponder()
    }
    
    
    

}
