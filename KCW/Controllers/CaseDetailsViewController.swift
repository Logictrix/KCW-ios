//
//  CaseDetailsViewController.swift
//  KCW
//
//  Created by Ankur sharma on 27/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class CaseDetailsViewController: UIViewController
{
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var viewDetails: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewTime.layer.borderWidth = 1
        viewTime.layer.borderColor = UIColor.lightGray.cgColor
        viewDetails.layer.borderWidth = 1
        viewDetails.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = false
    }

}
