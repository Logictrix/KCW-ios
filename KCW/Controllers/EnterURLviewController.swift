//
//  EnterURLviewController.swift
//  KCW
//
//  Created by Ankur sharma on 17/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class EnterURLviewController: UIViewController
{
    @IBOutlet weak var txtUrl: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var rememberMebtn: UIButton!
    var password = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        txtUrl.leftViewMode = UITextFieldViewMode.always
        txtPassword.leftViewMode = UITextFieldViewMode.always
        let bottomLineUrl = CALayer()
        bottomLineUrl.frame = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
        bottomLineUrl.backgroundColor = UIColor.black.cgColor
        txtUrl.borderStyle = UITextBorderStyle.none
        txtUrl.layer.addSublayer(bottomLineUrl)
        let bottomLinePass = CALayer()
        bottomLinePass.frame = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
        bottomLinePass.backgroundColor = UIColor.black.cgColor
        txtPassword.borderStyle = UITextBorderStyle.none
        txtPassword.layer.addSublayer(bottomLinePass)
        btnDone.layer.cornerRadius = 24
        ServiceManagerClass.baseUrl = txtUrl.text!
        if UserDefaults.standard.value(forKey: "baseUrl") != nil
        {
            ServiceManagerClass.baseUrl = UserDefaults.standard.value(forKey: "baseUrl") as! String
        }
        txtUrl.text = ServiceManagerClass.baseUrl
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = false
    }

    func callAPI()
    {
        ServiceManagerClass.baseUrl = txtUrl.text!
        self.password = txtPassword.text!
        APIController.urlauthentication(pass: password) { (response) in
            print(response)
            
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    
    
    @IBAction func btnDoneAction(_ sender: Any)
    {
        UserDefaults.standard.set(self.txtUrl.text, forKey: "baseUrl")
        UserDefaults.standard.synchronize()
        callAPI()
        
    }
    
    @IBAction func btnRemember(_ sender: Any)
    {
        
    }
    
    
}
