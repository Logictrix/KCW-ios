//
//  AddCalendarViewController.swift
//  KCW
//
//  Created by Ankur sharma on 25/08/18.
//  Copyright © 2018 Ankur sharma. All rights reserved.
//

import UIKit

class AddCalendarViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate
{
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtStartTime: UITextField!
    @IBOutlet weak var txtEndTime: UITextField!
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var txtCaseNumber: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    var myPickerView : UIPickerView!
    var pickerData : [String] = []
    var staffNum = ""
    var data : [JSON] = []
    var pcode = ""
    var caseNum = ""
    var firstName = ""
    var lastName = ""
    var openDate = ""
    var caseNumToShow = ""
    var caseValue = ""
    let datePicker = UIDatePicker()
    var activeTextField = UITextField()
    var availability = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if UserDefaults.standard.value(forKey: "staffNum") != nil
        {
            staffNum = UserDefaults.standard.value(forKey: "staffNum") as! String
        }
        submitBtn.layer.cornerRadius = 18
        txtCaseNumber.delegate = self
        txtDate.delegate = self
        txtStartTime.delegate = self
        txtEndTime.delegate = self
    }

    override func viewWillAppear(_ animated: Bool)
    {
        navigationController?.navigationBar.isHidden = false
        callCaseAPI()
    }
    
    func callCaseAPI()
    {
        APIController.getCase(staffno: staffNum)
        {
            (response) in
            print(response)
            self.data = response["data"].array!
            
            for dict in self.data
            {
                self.pcode = dict["pcode"].stringValue
                self.caseNum = dict["casenum"].stringValue
                self.firstName = dict["cfname"].stringValue
                self.lastName = dict["clname"].stringValue
                self.openDate = dict["dopen"].stringValue
                self.caseValue = "C" + " " + self.pcode + ":" + " " + self.firstName + " " + self.lastName + "(" + "Code:" + " " + self.caseNum
                self.pickerData.append(self.caseValue)
            }
        }
    }
    
    func callSaveCalendarAPI()
    {
        APIController.saveCalendar(startdate: txtDate.text!, starttime: txtStartTime.text!, enddatetime: txtEndTime.text!, subject: txtSubject.text!, casenum: txtCaseNumber.text!, userid: staffNum) { (response) in
            print(response)
        }
    }
    
    func pickUp(_ textField : UITextField)
    {
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        activeTextField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        activeTextField.inputAccessoryView = toolBar
    }
    
    func showDatePicker()
    {
        datePicker.minimumDate = NSDate() as Date
        //Formate Date
        if activeTextField == txtDate
        {
            datePicker.datePickerMode = .date
        }
        if activeTextField == txtStartTime
        {
            datePicker.datePickerMode = .time
        }
        if activeTextField == txtEndTime
        {
            datePicker.datePickerMode = .time
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        if activeTextField == txtDate
        {
            txtDate.inputAccessoryView = toolbar
            // add datepicker to textField
            txtDate.inputView = datePicker
        }
        if activeTextField == txtStartTime
        {
            txtStartTime.inputAccessoryView = toolbar
            // add datepicker to textField
            txtStartTime.inputView = datePicker
        }
        if activeTextField == txtEndTime
        {
            txtEndTime.inputAccessoryView = toolbar
            // add datepicker to textField
            txtEndTime.inputView = datePicker
        }
        
    }
    
    @objc func donedatePicker()
    {
        //For date formate
        if activeTextField == txtDate
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            txtDate.text = formatter.string(from: datePicker.date)
            //dismiss date picker dialog
            self.availability = txtDate.text!
            self.view.endEditing(true)
        }
        if activeTextField == txtStartTime
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            txtStartTime.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
        if activeTextField == txtEndTime
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            txtEndTime.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker()
    {
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil
        {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "Helvetica", size: 17)
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = pickerData[row]
        pickerLabel?.textColor = UIColor.black
        return pickerLabel!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.caseNumToShow = pickerData[row].components(separatedBy: ":").first!
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.activeTextField = textField
        if activeTextField == txtDate
        {
            self.showDatePicker()
        }
        if self.activeTextField == txtCaseNumber
        {
            self.pickUp(self.txtCaseNumber)
        }
        if self.activeTextField == txtStartTime
        {
            self.showDatePicker()
        }
        if self.activeTextField == txtEndTime
        {
            self.showDatePicker()
        }
    }
    
    @objc func doneClick()
    {
        self.txtCaseNumber.text = self.caseNumToShow
        activeTextField.resignFirstResponder()
    }
    
    @objc func cancelClick()
    {
        activeTextField.resignFirstResponder()
    }
    
    @IBAction func SubmitAction(_ sender: Any)
    {
        callSaveCalendarAPI()
    }
    
    //MARK:- convert timeStamp to Date
    
    func stringToDateString(datestring:String) -> String
    {
        let date = NSDate(timeIntervalSince1970: Double(datestring)! / 1000)
        let formatter = DateFormatter()
        // formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "dd-MMM-yyyy"
        //formatter.dateFormat = "E, dd MMM yyyy HH:mm:ss"
        print(formatter.string(from: date as Date))
        return formatter.string(from: date as Date)
    }
    
    //MARK:- convert Date to timeStamp
    
    
    
}
